from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.template import loader
from django.contrib.auth import logout
from .models import Contenido

# Create your views here.

formulario = """
<p><form action="" method= "POST">
    Valor: <input type="text" name="valor"><br/>
    <input type="submit" value="Enviar">
</form>
"""


@csrf_exempt
def get_content(request, llave):
    if request.method == "PUT":
        valor = request.body.decode('utf-8')
        try:
            c = Contenido(clave=llave, valor=valor)
            valor = c.valor
        except Contenido.DoesNotExist:
            c = Contenido(clave=llave, valor=valor)
        c.save()

    if request.method == "POST":
        # coger lo que haya en el cuerpo de HTTP
        valor = request.POST['valor']
        # guardarlo en la base de datos
        c = Contenido(clave=llave, valor=valor)
        c.save()

    try:
        contenido = Contenido.objects.get(clave=llave)
        respuesta = "El valor de la clave " + contenido.clave + " es " + contenido.valor
    except Contenido.DoesNotExist:
        if request.user.is_authenticated:
            respuesta = formulario
        else:
            respuesta = "No puedes introducir nada si no estas autenticado. <a href='/login'>Haz login!</a>"
    return HttpResponse(respuesta)


def index(request):
    content_list = Contenido.objects.all()
    template = loader.get_template('cms/index.html')
    context = {'content_list': content_list}
    return HttpResponse(template.render(context, request))


def loggedIn(request):
    if request.user.is_authenticated:
        respuesta = "Logged in as " + request.user.username
    else:
        respuesta = "Not logged in. <a href='/admin/'>Login via admin</a>"
    return HttpResponse(respuesta)


def logout_view(request):
    logout(request)
    return redirect('/cms/')
